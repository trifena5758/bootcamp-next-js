// import Container from "@component/organisms/Container";
import Image from "next/image";
import TestRedux from "./testRedux";

export const metadata = {
  title: "About Us",
};

export default function About() {
  return (
    // <Container>
    <div className="min h-screen">
      <h1 className="text-5xl">About</h1>
      <TestRedux />
    </div>
    // </Container>
  );
}
