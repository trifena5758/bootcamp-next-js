"use client";

import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import { useForm } from "react-hook-form";
import React from "react";
import SubmitForm from "./submitForm";
import { useRouter } from "next/navigation";

export interface FormLogin {
  email: string;
  password: string;
}

const schema = yup.object({
  email: yup
    .string()
    .email("Formatnya gak sesuai dik")
    .required("Emailnya dicek lagi dik")
    .matches(
      /^(?![\w\.@]*\.\.)(?![\w\.@]*\.@)(?![\w\.]*@\.)\w+[\w\.]*@[\w\.]+\.\w{2,}$/i,
      "Formatnya gak sesuai dik"
    ),
  password: yup.string().required("Passwordnya diisi dik"),
});

export default function FormLogin() {
  const router = useRouter();
  const form = useForm<FormLogin>({
    defaultValues: {
      email: "",
      password: "",
    },
    resolver: yupResolver(schema),
    mode: "onChange", // kapan ketika validasi itu akan dipanggil
  });
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = form;

  const onSubmited = (data: FormLogin) => {
    console.log(data);
    SubmitForm(data)
      .then((res) => {
        console.log(res);
        router.push("/");
      })
      .catch(() => {
        alert("username atau password salah");
      });
  };

  return (
    <div className="flex flex-col custom-shadow p-5 w-[300px] gap-5 mx-auto border-blue-900 border-2 rounded-md bg-blue-200">
      <form onSubmit={handleSubmit(onSubmited)} className="flex flex-col gap-3">
        <h1 className="font-bold">Email</h1>
        <input
          type="email"
          className="border-blue-900 border-2 rounded-md p-2"
          placeholder="Email"
          {...register("email")}
        />
        <p className="text-red-700 text-sm">{errors.email?.message}</p>

        <h1 className="font-bold">Password</h1>
        <input
          type="password"
          className="border-blue-900 border-2 rounded-md p-2"
          placeholder="Password"
          {...register("password")}
        />
        <p className="text-red-700 text-sm">{errors.password?.message}</p>

        <button
          type="submit"
          className="bg-pink-900 p-2 rounded-md font-bold text-white"
        >
          Login
        </button>
      </form>
    </div>
  );
}
