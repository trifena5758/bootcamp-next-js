import React from "react";
import FormLogin from "./formLogin";

export const metadata = {
  title: "Login",
};

export default function Login() {
  return (
    <div className="min-h-screen w-screen flex justify-center items-center">
      <FormLogin />
    </div>
  );
}
