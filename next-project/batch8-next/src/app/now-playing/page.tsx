import Image from "next/image";
import ListId from "./listId";

export const metadata = {
  title: "NowPlaying",
};
export default function NowPlaying() {
  return (
    //<Container>
    <div className="min-h-screen px-14 pt-14">
      <ListId />
    </div>
    //</Container>
  );
}
