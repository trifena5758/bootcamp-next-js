// import Container from "@component/organisms/Container";
import Image from "next/image";
import { cookies } from "next/headers";

export default function Home() {
  const auth = cookies().get("_userInfo");
  return (
    // <Container>
    <div className="min h-screen">
      <h1 className="text-5xl">Home</h1>
      <h1>{auth?.value}</h1>
    </div>
    // </Container>
  );
}
