const IMAGES = {
  logoBerijalan: "/img/logo.png",
  logoBerijalanLight: "/img/logo-light.png",
};

export default IMAGES;
